﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Carrinho_Ecommerce.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<style type="text/css">
    .style1
    {
        width: 900px;
    }
    .style2
    {
        width:633px;
        text-align:left;
    }
    .style3
    {
        width:257px;
        text-align:center;
    }
    .style4
    {
        width: 185px;
        text-align:center;
    }
    .style6
    {
        width:260px;
        text-align:left;
    }
    .style7
    {
        width:427px;
        text-align: center;
    }
    .style8
    {
        width: 108px;
        text-align:center;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">

        </asp:ScriptManager>
   
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table style="text-align:center"" class="style1">
                    <tr>
                        <td>
                            <table class="style1" style="text-align:center; border-bottom-style:ridge; border-width: medium; border-color:#9933FF">
                                <tr>
                                    <td class="style8" style="text-align:center" rowspan="2">
                                        <asp:Image ID="sacola" runat="server" Height="53px" ImageUrl="~/Imagens/sacola.png" Width="72px" />
                                        &nbsp;
                                    </td>
                                    <td class="style6" rowspan="2">
                                        <asp:LinkButton ID="lblLogo" runat="server" Text="Carrinho Ecommerce" Font-Names="Eras Demi ITC" Font-Size="20" ForeColor="#6600CC" OnClick="lblLogo_Click"> </asp:LinkButton>
                                        <br />
                                        Sistema de ecommerce Utilizando C#/ASP.NET
                                    </td>
                                  
                                    <!--  <td class="style7" rowspan="2">
                                        <asp:Image ID="Imagem3" runat="server" Height="67px" ImageUrl="~/Imagens/LogoNormal.png" width="282px"/>
                                    </td> -->

                                    <td rowspan="2" style="text-align:right">
                                        <asp:Image ID="Imagem2" runat="server" Height="53px" ImageUrl="~/Imagens/LogoNormal.png" Width="70px"/>
                                    </td>
                                    <td style="text-align:left">
                                        <asp:LinkButton ID="btnCarrinhoEcommerce" runat="server" Font-Underline="false" Font-Size="20px" ForeColor="Red" OnClick="btnCarrinhoEcommerce_Click">0
                                        </asp:LinkButton>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3" style="vertical-align:middle">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="text-align:center;border: thin ridge #9900FF" class="style1" >
                                <tr>
                                    <td class="style2">
                                        &nbsp;
                                        <asp:Label ID="lblNomeCategoria" runat="server" Font-Size="15" ForeColor="#6600CC"></asp:Label>
                                    </td>
                                    <td class="style3" style="border-left-style: ridge; border-width: thin; border-color: #9900FF">
                                        &nbsp;
                                        <asp:Label ID="lblProducts" runat="server" Text="Produtos" Font-Size="15" ForeColor="#6600CC"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="text-align:center" class="style1">
                                <tr>
                                    <td class="style2" style="vertical-align:top">
                                        <asp:Panel ID="pnlProdutos" runat="server" ScrollBars="Auto" Height="500px" BorderColor="Black" BorderStyle="Inset" BorderWidth="1px">
                                            <asp:DataList ID="dlProdutos" runat="server" 
                                                RepeatColumns="3" width="600px" Font-Bold="False" 
                                                Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
                                                <ItemTemplate>
                                                    <div style="text-align:left">
                                                        <table class="style4" style="border-spacing:1px; border:1px ridge #9900FF" >
                                                            <tr>
                                                                <td style="border-bottom-style: ridge; border-width: 1px; border-color:#000000">
                                                                    <asp:Label ID="lblNomeProduto" runat="server" text='<%# Eval("Nome")%>' style="font-weight:700"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <img alt="" src="<%# Bind('ImagemUrl')%>" runat="server" id="ImagemProdutoFoto" style="border: ridge 1px black; width: 173px; height:160px;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Price:<asp:Label ID="lblPreco" runat="server" Text="<%# Bind('Preco')%>"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnAdicionarAoCarrinho" runat="server" CommandArgument="<%# Bind('IDProduto') %>" 
                                                                        Onclick="btnAdicionarAoCarrinho_Click" text="Adicionar" 
                                                                        Width="100%" BorderColor="Black" BorderStyle="Inset" BorderWidth="1px" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div> 
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlMeuCarrinho" runat="server" ScrollBars="Auto" Height="500px" 
                                            BorderColor="Black" BorderStyle="Inset" BorderWidth="1px" visible="false" >
                                            <table style="text-align:center; border-spacing:1px;">
                                                <tr>
                                                    <td style="text-align:center;">
                                                        <asp:DataList ID="dlProdutosCarrinho" runat="server" RepeatColumns="3" Font-Bold="false"
                                                            Font-Italic="false" Font-Overline="false" font-strikeout="false" font-underline="false"
                                                            width="551px">
                                                            <ItemTemplate>
                                                                <div style="text-align:left">
                                                                    <table style="border-spacing:1px; border: 1px ridge #9900FF; text-align:center; width:172px" class="style4">
                                                                        <tr>
                                                                            <td style="border-bottom-style:ridge; border-bottom-width:1px; border-color: #000000">
                                                                                <asp:Label ID="lblNomeProduto" runat="server" Text='<%# Eval("Name")%>' Style="font-weight: 700"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            <img alt="" src="<%# Bind('ImageUrl') %>" runat="server" id="imgProdutoFoto" style="border: ridge 1px black; width: 112px; height:100px;" />"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Price:<asp:Label ID="lblPrice" runat="server" Text="<%# Bind('Preco') %>"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btnRemoverDoCarrinho" runat="server" CommandArgument="<%# Bind('IDProduto') %>" Text="RemoverDoCarrinho" width="100%" borderColor="Black" BorderStyle="Inset" 
                                                                                    BorderWidth="1px" OnClick="btnRemoverDoCarrinho_Click" 
                                                                                    />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ItemTemplate>

                                                        </asp:DataList>

                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td class="style3" style="vertical-align:top; text-align:center">
                                        <asp:Panel ID="pnlCategorias" runat="server" ScrollBars="Auto" Height="500px" 
                                            BorderColor="Black" BorderStyle="Inset" BorderWidth="1px">
                                            <asp:DataList ID="dlCategorias" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" 
                                                GridLines="Horizontal" Width="252px">
                                                <FooterStyle BackColor="#CCC99" ForeColor="Black" />
                                                <FooterStyle BackColor="333333" Font-Bold="True" ForeColor ="White" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnCategoria" runat="server" Text="<%# Bind('NomeCategoria')%>"
                                                        onclick="lbtnCategoria_Click" CommandArgument="<%# Bind('IDCategoria') %>"></asp:LinkButton>
                                                </ItemTemplate> 
                                                <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                            </asp:DataList>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:center; border: thin ridge #9900FF">
                                        &nbsp;&copy; Luciano Passos || <a href="Admin/Login.aspx">Painel do Administrador</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
         </form>
</body>
</html>
