﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Carrinho_Ecommerce.CamadaDados;
using System.Data;
using System.Data.SqlClient;


namespace Carrinho_Ecommerce.CamadaNegocios
{
    public class CarrinhoCompras
    {
        public string NomeCategoria;
        public int IDCategoria;

        public string NomeProduto;
        public string ImagemProduto;
        public string PrecoProduto;
        public string DescricaoProduto;
      
        public void AdicionarNovaCategoria()
        {
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = CamadaDados.DAL.AddParamater("NomeCategoria",NomeCategoria,System.Data.SqlDbType.VarChar, 200);
            DataTable dt = CamadaDados.DAL.ExecuteDTByProcedure("SP_AdicionarNovaCategoria", parameters);
        }
        public void AdicionarNovoProduto()
        {
            SqlParameter[] parameters = new SqlParameter[5];
            parameters[0] = CamadaDados.DAL.AddParamater("@NomeProduto", NomeProduto, System.Data.SqlDbType.VarChar, 500);
            parameters[1] = CamadaDados.DAL.AddParamater("@DescricaoProduto", DescricaoProduto, System.Data.SqlDbType.VarChar, 1000);
            parameters[2] = CamadaDados.DAL.AddParamater("@PrecoProduto", PrecoProduto, System.Data.SqlDbType.VarChar, 500);
            parameters[3] = CamadaDados.DAL.AddParamater("@ImagemUrl", ImagemProduto, System.Data.SqlDbType.VarChar, 500);
            parameters[4] = CamadaDados.DAL.AddParamater("@IDCategoria", IDCategoria, System.Data.SqlDbType.Int, 100);

            DataTable dt = CamadaDados.DAL.ExecuteDTByProcedure("SP_AdicionarNovoProduto",parameters);
        }

        // public DataTable GetTodosProdutos() { }

    public DataTable GetCategorias()
        {
            SqlParameter[] parameters = new SqlParameter[0];
            DataTable dt = CamadaDados.DAL.ExecuteDTByProcedure("SP_GetTodasCategorias", parameters);
            return dt;
        }
}
}       