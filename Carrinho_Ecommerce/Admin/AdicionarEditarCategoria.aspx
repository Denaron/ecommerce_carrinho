﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdicionarEditarCategoria.aspx.cs" Inherits="Carrinho_Ecommerce.Admin.AdicionarEditarCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div style="text-align:center">
    <h4>
        Adicionar nova categoria
    </h4>
        <hr />
    </div>
    <table  style=" padding:1px; align-content:center;width:100%;background-color:#FFFFFF;">
        <tr>
            <td style="width: 50%; padding-left: 100px;align-content:flex-start" >
                Category name:
            </td>
            <td style="width: 50%; align-content:flex-start">
                <asp:Textbox ID="txtNomeCategoria" runat="server" Width="212px"></asp:Textbox>
            </td>
        </tr>
        <tr>
        <td style="width: 50%; padding-left:100px; align-content:flex-start">
          &nbsp;
            </td>
            <td style="width:50px; align-content:flex-start">
                <asp:Button ID="btnSubmit" runat="server" Text="Enviar" Width="100px" Height="30px" OnClick="btnSubmit_Click"/>
            </td>
        </tr>
        <tr>
            <td style="width=50%;align-content:flex-end">
                &nbsp;
            </td>
        </tr>
        <tr>
             <td style="width=50%;align-content:flex-end">
                &nbsp;
            </td>
        </tr>

    </table>
</asp:Content>
