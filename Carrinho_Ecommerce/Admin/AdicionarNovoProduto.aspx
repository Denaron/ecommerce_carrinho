﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdicionarNovoProduto.aspx" Inherits="Carrinho_Ecommerce.Admin.AdicionarNovoProduto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div style="text-align:center">    
    <h4>
        Adicionar novo produto
    </h4>
         <table style="text-align:center; width: 898px;" class="nav-justified">
             <tr>
                 <td style="height: 20px; width: 449px">Nome do Produto:</td>
                 <td style="height: 20px; width: 449px;">
                     <asp:TextBox ID="txtNomeProduto" runat="server" Width="281px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;</td>
                 <td style="width: 449px">&nbsp;</td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Categoria do Produto:</td>
                 <td style="width: 449px">
                     <asp:DropDownList ID="ddlCategoria" runat="server" Height="16px" Width="289px">
                     </asp:DropDownList>
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;</td>
                 <td style="width: 449px">&nbsp;</td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Descrição do Produto:</td>
                 <td style="width: 449px">
                     <asp:TextBox ID="txtDescricaoProduto" runat="server" Height="73px" Width="280px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;</td>
                 <td style="width: 449px">&nbsp;</td>
             </tr>
             <tr>
                 <td style="width: 449px; height: 22px;">&nbsp;&nbsp;&nbsp; Imagem do produto:</td>
                 <td style="height: 22px; width: 449px">
                     <asp:FileUpload ID="updateFotoProduto" runat="server" Width="366px" />
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;</td>
                 <td style="width: 449px">&nbsp;</td>
             </tr>
             <tr>
                 <td style="height: 40px; width: 449px">Preço do produto:</td>
                 <td style="height: 40px; width: 449px;">
                     <asp:TextBox ID="txtPrecoProduto" runat="server" Width="277px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px; height: 40px;"></td>
                 <td style="width: 449px; height: 40px">
                     <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Enviar" Width="110px" Height="23px" />
                 </td>
             </tr>
             <tr>
                 <td style="width: 449px">&nbsp;</td>
                 <td style="width: 449px">
                     &nbsp;</td>
             </tr>
         </table>
    </div>


</asp:Content>
