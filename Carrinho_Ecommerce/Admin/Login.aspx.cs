﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Configuration;

namespace Carrinho_Ecommerce.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string LoginID = WebConfigurationManager.AppSettings["AdminLoginID"];
            string Password = WebConfigurationManager.AppSettings["AdminPassword"];

            if (txtLoginId.Text == LoginID && txtSenha.Text == Password)
            {
                Session["CarrinhoEcommerceAdmin"] = "CarrinhoEcommerceAdmin";
                Response.Redirect("~/Admin/AdicionarNovoProduto.aspx");
            }
            else
            {
                lblAlert.Text = "Login ou Senha estão incorretos.";
            }
                


        }
    }
}