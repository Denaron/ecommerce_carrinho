﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Carrinho_Ecommerce.CamadaDados;
using Carrinho_Ecommerce.CamadaNegocios;
namespace Carrinho_Ecommerce.Admin
{
    public partial class AdicionarNovoProduto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCategorias();
            }
        }

        private void GetCategorias()
        {
            CarrinhoCompras c = new CarrinhoCompras();
            DataTable dt = c.GetCategorias();
            if (dt.Rows.Count > 0)
            {
                ddlCategoria.DataValueField = "IDCATEGORIA";
                ddlCategoria.DataTextField = "NOMECATEGORIA";
                ddlCategoria.DataSource = dt;
                ddlCategoria.DataBind();
            }
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            if (updateFotoProduto.PostedFile != null)
            {
                SalvarFotoProduto();

                CarrinhoCompras c = new CarrinhoCompras()
                {
                    NomeProduto = txtNomeProduto.Text,
                    ImagemProduto = "~/ImagensProdutos/" + updateFotoProduto.FileName,
                    PrecoProduto = txtPrecoProduto.Text,
                    DescricaoProduto = txtDescricaoProduto.Text,
                    IDCategoria = Convert.ToInt32(ddlCategoria.SelectedValue)

                };
                c.AdicionarNovoProduto();
                //Alert.Show("Produto cadastrado com sucesso!");
                ClearText();

            }
            else
            {
                //Alert.Show("Adicione a foto do produto.");
            }
        }

        private void ClearText()
        {

        }

        private void SalvarFotoProduto()
        {
            if (updateFotoProduto.PostedFile != null)
            {
                string filename = updateFotoProduto.PostedFile.FileName.ToString();
                string fileExt = System.IO.Path.GetExtension(updateFotoProduto.FileName);

                //Checa tamanho do nome do produto
                if (filename.Length > 96)
                {

                }
                else if (fileExt != ".jpeg" && fileExt != ".jpg" && fileExt != ".png" && fileExt != ".bmp")
                {

                }
                else if (updateFotoProduto.PostedFile.ContentLength > 4000000)
                {

                }
                else
                {
                    updateFotoProduto.SaveAs(Server.MapPath("~/ImagensProdutos/"+ filename));
                }
            }
        }

    }
}