﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Carrinho_Ecommerce.CamadaNegocios;

namespace Carrinho_Ecommerce.Admin
{
    public partial class AdicionarEditarCategoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            CarrinhoCompras c = new CarrinhoCompras
            {
                NomeCategoria = txtNomeCategoria.Text
            };
            c.AdicionarNovaCategoria();
            txtNomeCategoria.Text = string.Empty;
            Response.Redirect("~/Admin/AdicionarNovoProduto.aspx");
        }
    }
}