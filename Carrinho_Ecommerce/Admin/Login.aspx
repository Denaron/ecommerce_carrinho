﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Carrinho_Ecommerce.Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tela login</title>
    <link href="../Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 200px;">
    
        <table class="style1" style="text-align:center; border: 1px ridge #999999; width:450px;">
            <tr>
                <td style="text-align:center" colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Names="Aharoni" ForeColor="#0033cc" Text="Painel do Administrador" 
                    Style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="text-align:center; width: 50%;">
                    ID Login: 
                </td>
                <td style="text-align:center;width: 50%;">
                    <asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox> 
                </td>
            </tr>
               <tr>
                <td  style="text-align:center;width: 50%;">
                    Senha:
                </td>
                <td style="text-align:center;width: 50%;">
                    <asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox> 
                </td>
            </tr>
        <tr>
            <td  style="text-align:center; width:50%;">
                &nbsp;
            </td>
            <td style="text-align:center;width: 50%;">
                <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
            </td>
        </tr>
            <tr>
                <td style="text-align:center" colspan="2">
                    <hr />
                    <asp:label ID="lblAlert" runat="server" Font-Names="Aharoni" Forecolor="Red"></asp:label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
